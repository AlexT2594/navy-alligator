# NavyAlligator

NavyAlligator is a Python project which, given a file composed of a list of
JSON entries that contain GPS coordinates, and a target GPS position, does the following:

- calculates the entries which are within 100km of the target position
- sorts the entries by User ID (ascending)

## File input example

```python
{"latitude": "54.133333", "user_id": 24, "name": "Rose Enright", "longitude": "-6.433333"}
{"latitude": "53.74452", "user_id": 29, "name": "Oliver Ahearn", "longitude": "-7.11167"}
{"latitude": "52.2559432", "user_id": 9, "name": "Jack Dempsey", "longitude": "-7.1048927"}
{"latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"}
{"latitude": "52.366037", "user_id": 16, "name": "Ian Larkin", "longitude": "-8.179118"}
```

## Usage
```python
na = NavyAlligator('./example/customers.txt', 53.339428, -6.257664)
entries = na.closest_entries(100)
'''
entries == [
    '{"latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"}',
    '{"latitude": "54.133333", "user_id": 24, "name": "Rose Enright", "longitude": "-6.433333"}',
    '{"latitude": "53.74452", "user_id": 29, "name": "Oliver Ahearn", "longitude": "-7.11167"}',
    ]
'''
```





## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)