import json

from models.Position import Position
from models.Entry import Entry

class NavyAlligator:
    """A class to find the closest entries to a given target"""

    def __init__(self, file_name:str, latitude:float=0, longitude:float=0):
        """
        Parameters
        ----------
        file_name : str
            The name of the input file. An exception is raised:
                - if the file doesn't exist
                - if the file is not properly formatted:
                    - each line is a JSON
                    - '\n' used as a line separator
        latitude : float
            Latitude of the target position (default is 0)
        longitude : float
            Longitude of the target position (default is 0)
        """
        self.target_position = Position(latitude, longitude)
        self.entries = []
        self.entry_to_input_line_dict = {}
        with open(file_name, 'r') as file:
            for line in file:
                line = line.strip()
                line_dict = json.loads(line)
                entry = Entry(
                        latitude=float(line_dict['latitude']),
                        longitude=float(line_dict['longitude']),
                        user_id=line_dict['user_id'],
                        name=line_dict['name']
                )
                self.entry_to_input_line_dict[entry] = line
                self.entries.append(entry)

    def closest_entries(self, max_distance:float=1):
        """
        Returns the closest entries with respect to max_distance ordered
        by user_id

        Parameters
        ----------
        max_distance : float
            The maximum distance in km for an entry to be considered (default is 1)
        """
        closest_entries = []
        lines = []

        for entry in self.entries:
            if self.target_position.distance(Position(entry.latitude, entry.longitude)) <= max_distance:
                closest_entries.append(entry)

        closest_entries.sort(key=lambda x: x.user_id)

        for entry in closest_entries:
            lines.append(self.entry_to_input_line_dict[entry])

        return lines

