from __future__ import annotations
from math import acos, sin, cos, radians

class Position:
    """Models a GPS position"""

    def __init__(self, latitude:float=0, longitude:float=0):
        """
        Parameters
        ----------
        latitude : float
            The latitude of a GPS position expressed in degrees (default is 0)
        longitude : float
            The longitude of a GPS position expressed in degrees (default is 0)
        """

        self.latitude = latitude
        self.longitude = longitude

    def distance(self, other:Position=None) -> float:
        """
        Parameters
        ----------
        other : Position
            The Position from which to calculate the distance. If no position
            is given it returns 0.
        """

        EARTH_RADIUS = 6371

        if other is None:
            return 0

        lat = radians(self.latitude)
        long = radians(self.longitude)
        
        lat_other = radians(other.latitude)
        long_other = radians(other.longitude)

        long_delta = abs(long - long_other)

        try:
            central_angle = acos(sin(lat)*sin(lat_other) + cos(lat)*cos(lat_other)*cos(long_delta))
        except ValueError:
            # when trying to find distance between 2 very close points
            central_angle = 0

        return EARTH_RADIUS * central_angle


        
