class Entry:
    """A class to represent a single JSON line"""

    def __init__(self, latitude:float=0, longitude:float=0, user_id:str="", name:str=""):
        """
        Parameters
        ----------
        latitude : float
            The latitude of an entry (default is 0)
        longitude : float
            The longitude of an entry (default is 0)
        user_id : str
            The User ID of an entry (default is "")
        name : str
            The name of an entry (default is "")
        """

        self.latitude = latitude
        self.longitude = longitude
        self.user_id = user_id
        self.name = name