import unittest

from app.NavyAlligator import NavyAlligator

class TestModels(unittest.TestCase):

    def test_no_args(self):
        with self.assertRaises(TypeError):
            NavyAlligator()

    def test_file_not_exist(self):
        with self.assertRaises(FileNotFoundError):
            NavyAlligator('')

    def test_navy(self):
        na = NavyAlligator('./tests/unit/input.txt', 53.339428, -6.257664)
        entries = na.closest_entries(100)

        expected_entries= [
            '{"latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"}',
            '{"latitude": "54.133333", "user_id": 24, "name": "Rose Enright", "longitude": "-6.433333"}',
            '{"latitude": "53.74452", "user_id": 29, "name": "Oliver Ahearn", "longitude": "-7.11167"}',
        ]

        for entry, expected_entry in zip(entries, expected_entries):
            if entry != expected_entry:
                self.fail("entry is different than expected entry: \nentry: " + entry + "\nexpected entry: " + expected_entry)




if __name__ == "__main__":
    unittest.main()