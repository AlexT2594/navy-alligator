import unittest

from models.Entry import Entry
from models.Position import Position

class TestModels(unittest.TestCase):
    POSITION = Position(53.339428, -6.257664)

    def test_entry_no_args(self):
        try:
            Entry()
        except:
            self.fail("testing Entry without args")

    def test_position_no_args(self):
        try:
            Position()
        except:
            self.fail("testing Position without args")

    def test_position_distance_no_args(self):
        self.assertAlmostEqual(TestModels.POSITION.distance(), 0)


    def test_position_distance(self):
        other_position = Position(52.986375, -6.043701)

        self.assertAlmostEqual(TestModels.POSITION.distance(other_position), 41.768, 2)

if __name__ == "__main__":
    unittest.main()