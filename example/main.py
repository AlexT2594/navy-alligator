from app.NavyAlligator import NavyAlligator
from models.Position import Position

def main():
    na = None
    try:
        na = NavyAlligator('./example/customers.txt', 53.339428, -6.257664)
    except FileNotFoundError:
        print("File not found")
        return
    except Exception as e:
        print(e)
        return
    
    entries = na.closest_entries(100)

    with open('output.txt', 'w') as file:
        file.writelines([entry + "\n" for entry in entries])

    return

if __name__ == "__main__":
    main()